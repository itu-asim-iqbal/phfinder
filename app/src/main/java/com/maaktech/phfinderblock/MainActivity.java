package com.maaktech.phfinderblock;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static android.support.v4.graphics.ColorUtils.RGBToLAB;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class MainActivity extends AppCompatActivity
{
    TextureView mTextureView;
    CameraDevice mCameraDevice;
    String mCameraId;
    HandlerThread mBackgroundHandlerThread;
    Handler mBackgroundHandler;
    Size mPreviewSize;
    CaptureRequest.Builder mCaptureRequestBuilder;
    Bitmap[] bitmapBuffer;
    ImageButton captureTestButton;


    Switch modeSwitch;
    TextView phValueText;
    TextView countTextView;
    Button mFlashButton;
    EditText ipAddressTxt;
    EditText numberOfPicturesTxt;
    Button originalColorButton;
    Button modColorButton;

    public static int port = 5678;

    public int count;

    int screenWidth;
    int screenHeight;
    public static int numberOfPictures = 15;//150;
    public static final int delay = 200;
    public static final float focusLength = 10.16f; // centimeters
    public static final float focus = 100.0f/focusLength;
    public static final int halfGridWidth = 50;
    public double value_avg, value_mod;
    public double first, second;
    public boolean serverConnected = false;
    public DataOutputStream dos;
    public String colorStringOriginal, colorStringMod;
    public static Boolean flash = false;

    static SparseIntArray ORIENTATIONS = new SparseIntArray();

    public static final int[][] originalRefColors = {
                                                        {237,  31,  36}, // #ED1F24 - RED
                                                        {105, 189,  69}, // #69BD45 - GREEN
                                                        { 57,  83, 164}, // #3953A4 - BLUE
                                                        {  0,   0,   0}, // #000000 - BLACK
                                                        {255, 255, 255}, // #FFFFFF - WHITE
                                                        {246, 235,  22}, // #F6EB16 - YELLOW
                                                    };
//    public static final int[][] originalRefColors = {
//                                                        {255,   0,   0},
//                                                        {  0, 255,   0},
//                                                        {  0,   0, 255},
//                                                        {  0,   0,   0},
//                                                        {255, 255, 255},
//                                                        {127, 127, 127},
//                                                    };

    static
    {
        ORIENTATIONS.append(Surface.ROTATION_0, 0);
        ORIENTATIONS.append(Surface.ROTATION_90, 90);
        ORIENTATIONS.append(Surface.ROTATION_180, 180);
        ORIENTATIONS.append(Surface.ROTATION_270, 270);
    }

    static class compareSizeByArea implements Comparator<Size>
    {

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public int compare(Size lhs, Size rhs)
        {
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() / (long) rhs.getWidth() * rhs.getHeight());
        }
    }

    CameraDevice.StateCallback mCameraDeviceStateCallbackListener = new CameraDevice.StateCallback()
    {
        @Override
        public void onOpened(@NonNull CameraDevice cameraDevice)
        {
            mCameraDevice = cameraDevice;
            startPreview();
            /*Toast.makeText(getApplicationContext(), "Camera Fully Connected", Toast.LENGTH_SHORT).show();*/
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice)
        {
            cameraDevice.close();
            mCameraDevice = null;
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int i)
        {
            cameraDevice.close();
            mCameraDevice = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        screenHeight = displayMetrics.heightPixels;
        screenWidth = displayMetrics.widthPixels;

        CalibrationCanvas canvas = new CalibrationCanvas(getApplicationContext(), screenWidth, screenHeight);
        addContentView(canvas, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        phValueText = (TextView) findViewById(R.id.tvPhValue);

        mTextureView = (TextureView) findViewById(R.id.textureView);

//        bitmapBuffer = new Bitmap[numberOfPictures];

        captureTestButton = (ImageButton) findViewById(R.id.btnCapture);
        captureTestButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                captureTest();
            }
        });

        countTextView = (TextView) findViewById(R.id.txtCount);

        mFlashButton = (Button) findViewById(R.id.btnFlash);

        mFlashButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                flash = !flash;
                startPreview();
            }
        });
        ipAddressTxt = (EditText) findViewById(R.id.txtIp);
        numberOfPicturesTxt = (EditText) findViewById(R.id.txtNumberOfPictures);
        numberOfPicturesTxt.setText("" + numberOfPictures);

        originalColorButton = (Button) findViewById(R.id.btnOriginalColor);
        modColorButton = (Button) findViewById(R.id.btnModColor);

        modeSwitch = findViewById(R.id.switchMode);


    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onWindowFocusChanged(boolean hasChanged)
    {
        super.onWindowFocusChanged(hasChanged);
        View decorView = getWindow().getDecorView();
        if (hasChanged)
        {
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        closeCamera();
        stopBackgroundThread();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onResume()
    {
        super.onResume();
        startBackgroundThread();
        if (mTextureView.isAvailable())
        {
            setupCamera(screenWidth, screenHeight);
            connectCamera();
        }
        else
        {
            mTextureView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener()
            {
                @Override
                public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height)
                {
                    setupCamera(screenWidth, screenHeight);
                    connectCamera();
                }

                @Override
                public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1)
                {

                }

                @Override
                public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture)
                {
                    return false;
                }

                @Override
                public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture)
                {

                }
            });
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    void setupCamera(int width, int height)
    {
        CameraManager cameraManager = (CameraManager) getSystemService(CAMERA_SERVICE);
        try
        {
            for (String cameraId : cameraManager.getCameraIdList())
            {
                CameraCharacteristics cameraCharacteristics = cameraManager.getCameraCharacteristics(cameraId);
                if (cameraCharacteristics.get(CameraCharacteristics.LENS_FACING) == CameraCharacteristics.LENS_FACING_FRONT)
                {
                    continue;
                }

                StreamConfigurationMap map = cameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

                int deviceOrientation = getWindowManager().getDefaultDisplay().getRotation();
                int totalRotation = sensorToDeviceRotation(cameraCharacteristics, deviceOrientation);
                boolean swapRotation = totalRotation == 90 || totalRotation == 270;
                int rotatedWidth = width;
                int rotatedHeight = height;
                if (swapRotation)
                {
                    rotatedHeight = width;
                    rotatedWidth = height;
                    Log.e("SWAP", "TRUE");
                }
                mPreviewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class), rotatedWidth, rotatedHeight);
                mCameraId = cameraId;
                Log.e("CAMERA ID", mCameraId);
                return;
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    void connectCamera()
    {
        CameraManager cameraManager = (CameraManager) getSystemService(CAMERA_SERVICE);
        try
        {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            {
                if(ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
                {
                    cameraManager.openCamera(mCameraId, mCameraDeviceStateCallbackListener, mBackgroundHandler);
                }
                else
                {
                    requestPermissions(new String[] {Manifest.permission.CAMERA}, 0);
                }
            }
            else
            {
                cameraManager.openCamera(mCameraId, mCameraDeviceStateCallbackListener, mBackgroundHandler);
            }
        }
        catch (CameraAccessException e)
        {
            e.printStackTrace();
        }
    }

    void startPreview()
    {
        SurfaceTexture surfaceTexture = mTextureView.getSurfaceTexture();
        surfaceTexture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
        final Surface previewSurface = new Surface(surfaceTexture);
        try
        {
            mCaptureRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            mCaptureRequestBuilder.addTarget(previewSurface);
            mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_OFF);
            mCaptureRequestBuilder.set(CaptureRequest.LENS_FOCUS_DISTANCE, focus);
            if(flash)
                mCaptureRequestBuilder.set(CaptureRequest.FLASH_MODE, CaptureRequest.FLASH_MODE_TORCH);
            mCameraDevice.createCaptureSession(Arrays.asList(previewSurface),
                    new CameraCaptureSession.StateCallback()
                    {

                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession)
                        {
                            try
                            {
//                                mCaptureRequestBuilder.set(CaptureRequest.FLASH_MODE, CaptureRequest.FLASH_MODE_TORCH);
                                cameraCaptureSession.setRepeatingRequest(mCaptureRequestBuilder.build(), new CameraCaptureSession.CaptureCallback()
                                {
                                    @Override
                                    public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result)
                                    {
                                        super.onCaptureCompleted(session, request, result);
                                    }
                                }, mBackgroundHandler);
                            }
                            catch (CameraAccessException e)
                            {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession)
                        {
                            Toast.makeText(getApplicationContext(), "Camera Failure. Cannot Create Capture Session", Toast.LENGTH_SHORT).show();
                        }
                    }, null);
        }
        catch (CameraAccessException e)
        {
            e.printStackTrace();
        }
    }

    private void closeCamera()
    {
        if(mCameraDevice != null)
        {
            mCameraDevice.close();
            mCameraDevice = null;
        }
    }

    void startBackgroundThread()
    {
        mBackgroundHandlerThread = new HandlerThread("CameraApp");
        mBackgroundHandlerThread.start();
        mBackgroundHandler = new Handler(mBackgroundHandlerThread.getLooper());
    }

    void stopBackgroundThread()
    {
        mBackgroundHandlerThread.quitSafely();
        try
        {
            mBackgroundHandlerThread.join();
            mBackgroundHandlerThread = null;
            mBackgroundHandler = null;
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
//        mBackgroundHandler = new Handler(mBackgroundHandlerThread.getLooper());
    }

    static int sensorToDeviceRotation(CameraCharacteristics cameraCharacteristics, int deviceOrientation)
    {
        int sensorOrientation = cameraCharacteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
        deviceOrientation = ORIENTATIONS.get(deviceOrientation);
        return (sensorOrientation + deviceOrientation + 360) % 360;
    }

    static Size chooseOptimalSize(Size[] choices, int width, int height)
    {
        List<Size> bigEnough = new ArrayList<Size>();
        for(Size option : choices)
        {
            if(option.getHeight() == option.getWidth() * height/width && option.getWidth() >= width && option.getHeight() >= height)
            {
                bigEnough.add(option);
            }
        }
        if(bigEnough.size() > 0)
        {
            return Collections.min(bigEnough, new compareSizeByArea());
        }
        return choices[0];
    }

    private void captureTest()
    {
        Thread captureThread = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                numberOfPictures = Integer.parseInt(numberOfPicturesTxt.getText().toString());

                count = 1;
//                bitmapBuffer = new Bitmap[numberOfPictures];
                int[] r = new int[numberOfPictures];
                int[] g = new int[numberOfPictures];
                int[] b = new int[numberOfPictures];
                Bitmap tempBitmap;

                int sum_r;
                int sum_g;
                int sum_b;
                int pixel;

                int[] ref_r = new int[6];
                int[] ref_g = new int[6];
                int[] ref_b = new int[6];

                for(int i = 0; i < numberOfPictures; i++)
                {
//                    bitmapBuffer[i] = mTextureView.getBitmap(720, 1280);//576, 1024
                    tempBitmap = mTextureView.getBitmap();//720, 1280
//                    Log.e("COUNT", "" + count);
                    runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            countTextView.setText("" + count++);
                        }
                    });

//                    int temp_pixel = tempBitmap.getPixel(tempBitmap.getWidth() / 2, tempBitmap.getHeight() / 2);
//                    Log.e("RED", "" + Color.red(temp_pixel));
//                    Log.e("GREEN", "" + Color.green(temp_pixel));
//                    Log.e("BLUE", "" + Color.blue(temp_pixel));

                    sum_r = 0;
                    sum_g = 0;
                    sum_b = 0;

                    if(i == 0) // This condition is used to pick up the reference chart
                    {
                        int[] ref_pixels = new int[6];
                        ref_pixels[0] = tempBitmap.getPixel(CalibrationCanvas.refDistanceX, (int)((screenHeight / 2f) - (2.5f * CalibrationCanvas.centimeter)));
                        ref_pixels[1] = tempBitmap.getPixel(CalibrationCanvas.refDistanceX, (int)((screenHeight / 2f) - (1.5f * CalibrationCanvas.centimeter)));
                        ref_pixels[2] = tempBitmap.getPixel(CalibrationCanvas.refDistanceX, (int)((screenHeight / 2f) - (0.5f * CalibrationCanvas.centimeter)));
                        ref_pixels[3] = tempBitmap.getPixel(CalibrationCanvas.refDistanceX, (int)((screenHeight / 2f) + (0.5f * CalibrationCanvas.centimeter)));
                        ref_pixels[4] = tempBitmap.getPixel(CalibrationCanvas.refDistanceX, (int)((screenHeight / 2f) + (1.5f * CalibrationCanvas.centimeter)));
                        ref_pixels[5] = tempBitmap.getPixel(CalibrationCanvas.refDistanceX, (int)((screenHeight / 2f) + (2.5f * CalibrationCanvas.centimeter)));
                        ref_r[0] = Color.red(ref_pixels[0]);
                        for(int m = 0; m < 6; m++)
                        {
                            ref_r[m] = Color.red(ref_pixels[m]);
                            ref_g[m] = Color.green(ref_pixels[m]);
                            ref_b[m] = Color.blue(ref_pixels[m]);
                        }
                    }

                    for(int j = (tempBitmap.getWidth() / 2) - halfGridWidth; j <= (tempBitmap.getWidth() / 2) + halfGridWidth; j++)
                    {
                        for (int k = (tempBitmap.getHeight() / 2) - halfGridWidth; k <= (tempBitmap.getHeight() / 2) + halfGridWidth; k++)
                        {
                            pixel = tempBitmap.getPixel(j, k);
                            sum_r += Color.red(pixel);
                            sum_g += Color.green(pixel);
                            sum_b += Color.blue(pixel);
                        }
                    }
                    r[i] = sum_r / (((halfGridWidth * 2) + 1) * ((halfGridWidth * 2) + 1));
                    g[i] = sum_g / (((halfGridWidth * 2) + 1) * ((halfGridWidth * 2) + 1));
                    b[i] = sum_b / (((halfGridWidth * 2) + 1) * ((halfGridWidth * 2) + 1));
                    try
                    {
                        Thread.sleep(delay);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }




                String fileData = saveRGBs(r, g, b, ref_r, ref_g, ref_b);
//                sendToServer(fileData);


//                saveBitmaps();
                // here it goes :D
                int delta_red = 0;
                int delta_green = 0;
                int delta_blue = 0;
                for(int n = 0; n < 6; n++)
                {
                    delta_red = delta_red + originalRefColors[n][0] - ref_r[n];
                    delta_green = delta_green + originalRefColors[n][1] - ref_g[n];
                    delta_blue = delta_blue + originalRefColors[n][2] - ref_b[n];
                }
                delta_red = delta_red / 6;
                delta_green = delta_green / 6;
                delta_blue = delta_blue / 6;



                value_avg = 0;
                value_mod = 0;
                if(modeSwitch.isChecked())
                {
                    for (int l = r.length - 10; l < r.length; l++) // Predicting on the basis of last 10 pictures.
                    {
                        value_avg = value_avg + predictSaturation(r[l], g[l], b[l]);
                        value_mod = value_mod + predictSaturation(r[l] + delta_red, g[l] + delta_green, b[l] + delta_blue);
                    }
                }
                else
                {
                    for (int l = r.length - 10; l < r.length; l++) // Predicting on the basis of last 10 pictures.
                    {
                        value_avg = value_avg + predictPH(r[l], g[l], b[l]);
                        value_mod = value_mod + predictPH(r[l] + delta_red, g[l] + delta_green, b[l] + delta_blue);
                    }
                }

                value_avg = value_avg/10;
                value_mod = value_mod/10;

                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        DecimalFormat df = new DecimalFormat("#.00");
                        if(modeSwitch.isChecked())
                            phValueText.setText("Nitrogen = " + df.format(value_avg * 300) + " Kg/Acre");
                        else
                            phValueText.setText("PH Value = " + df.format(value_avg));
                        originalColorButton.setText(df.format(value_avg));
                        modColorButton.setText(df.format(value_mod));
                    }
                });


                delta_red = delta_red + r[r.length - 1];
                delta_green = delta_green + g[r.length - 1];
                delta_blue = delta_blue + b[r.length - 1];

                colorStringOriginal = "#";
                if(r[r.length - 1] < 16)
                    colorStringOriginal = colorStringOriginal + "0";
                colorStringOriginal = colorStringOriginal + Integer.toHexString(r[r.length - 1]);
                if(g[r.length - 1] < 16)
                    colorStringOriginal = colorStringOriginal + "0";
                colorStringOriginal = colorStringOriginal + Integer.toHexString(g[r.length - 1]);
                if(b[r.length - 1] < 16)
                    colorStringOriginal = colorStringOriginal + "0";
                colorStringOriginal = colorStringOriginal + Integer.toHexString(b[r.length - 1]);
                System.out.println("Original Color : " + colorStringOriginal);
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        originalColorButton.setBackgroundColor(Color.parseColor(colorStringOriginal));
                    }
                });

                colorStringMod = "#";
                if(delta_red < 16)
                    colorStringMod = colorStringMod + "0";
                colorStringMod = colorStringMod + Integer.toHexString(delta_red);
                if(delta_green < 16)
                    colorStringMod = colorStringMod + "0";
                colorStringMod = colorStringMod + Integer.toHexString(delta_green);
                if(delta_blue < 16)
                    colorStringMod = colorStringMod + "0";
                colorStringMod = colorStringMod + Integer.toHexString(delta_blue);
                System.out.println("Mod Color : " + colorStringMod);
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        modColorButton.setBackgroundColor(Color.parseColor(colorStringMod));
                    }
                });

                /*float[] originalHSV = rgb2hsv(r[r.length - 1], g[g.length - 1], b[b.length - 1]);
                float[] modHSV = rgb2hsv(delta_red, delta_green, delta_blue);
                Log.e("ORIGINAL HSV", "H : " + originalHSV[0] + " - S : " + originalHSV[1] + " - V : " + originalHSV[2]);
                Log.e("MOD HSV", "H : " + modHSV[0] + " - S : " + modHSV[1] + " - V : " + modHSV[2]);*/







            }
        });

        captureThread.start();
    }

    private void saveBitmaps()
    {
        SharedPreferences pref = getSharedPreferences("data", 0);
        int testNumber = pref.getInt("test_count", 0);
        SharedPreferences.Editor prefEditor = pref.edit();
        prefEditor.putInt("test_count", testNumber + 1);
        prefEditor.commit();
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/0-PH Finder/" + testNumber);
        myDir.mkdirs();


        try
        {
            for(int i = 0; i < bitmapBuffer.length; i++)
            {
                String fname = (i + 1 < 10?"0":"") + (i + 1) + ".jpg";
                File file = new File(myDir, fname);
                FileOutputStream out = new FileOutputStream(file);
                bitmapBuffer[i].compress(Bitmap.CompressFormat.JPEG, 100, out);
                out.flush();
                out.close();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        bitmapBuffer = null;
        Log.e("IMAGES", "SAVED");
    }

    private String saveRGBs(int[] r, int[] g, int[] b, int[] ref_r, int[] ref_g, int[] ref_b)
    {
        SharedPreferences pref = getSharedPreferences("data", 0);
        int testNumber = pref.getInt("test_count", 0);
        SharedPreferences.Editor prefEditor = pref.edit();
        prefEditor.putInt("test_count", testNumber + 1);
        prefEditor.commit();
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/0-PH Finder/");
        myDir.mkdirs();

        String fileData = "";

        File rgbFile = new File(root + "/0-PH Finder/" + testNumber + ".txt");
        try
        {
            String rgbString;
            FileOutputStream FOS = new FileOutputStream(rgbFile);
            for(int i = 0; i < r.length; i++)
            {
                rgbString = "" + r[i] + " " + g[i] + " " + b[i] + " " + rgbToHex(r[i], g[i], b[i]) + "\n";

//                FOS.write(rgbString.getBytes());
                fileData = fileData + rgbString;
            }
            fileData = fileData + "|";
            for(int i = 0; i < ref_r.length; i++)
            {
                rgbString = "" + ref_r[i] + " " + ref_g[i] + " " + ref_b[i] + " " + rgbToHex(ref_r[i], ref_b[i], ref_g[i]) + "\n";
//                FOS.write(rgbString.getBytes());
                fileData = fileData + rgbString;
            }
//            Log.e("Data:\n", fileData);
            System.out.println(fileData);
//            Log.e("\n", fileData);
            FOS.write(fileData.getBytes());
            rgbString = "\b";
            FOS.write(rgbString.getBytes());
            FOS.close();
            fileData = fileData.substring(0, fileData.length() - 1);
            runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    Toast.makeText(getApplicationContext(), "Images Saved", Toast.LENGTH_SHORT).show();
                }
            });
            return fileData;
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    private void sendToServer(String data)
    {
        if(ipAddressTxt.getText().toString().isEmpty())
        {
//            Toast.makeText(getApplicationContext(), "Enter IP Address", Toast.LENGTH_SHORT).show();
            return;
        }
        try
        {
            if(!serverConnected)
            {
                Socket socket = new Socket(ipAddressTxt.getText().toString(), port);
                dos = new DataOutputStream(socket.getOutputStream());
                serverConnected = true;
            }
            dos.writeUTF(data);
//            dos.close();
//            socket.close();
//            Toast.makeText(getApplicationContext(), "Data Sent", Toast.LENGTH_SHORT).show();

            runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    Toast.makeText(getApplicationContext(), "Sent To Server", Toast.LENGTH_SHORT).show();
                }
            });

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    private double predictPH(int r,int g,int b)
    {
        int ph_rgb[][]=new int[15][3];
        ph_rgb[0][0]=238;
        ph_rgb[0][1]=28;
        ph_rgb[0][2]=37;
        ph_rgb[1][0]=242;
        ph_rgb[1][1]=103;
        ph_rgb[1][2]=36;
        ph_rgb[2][0]=248;
        ph_rgb[2][1]=198;
        ph_rgb[2][2]=17;
        ph_rgb[3][0]=245;
        ph_rgb[3][1]=237;
        ph_rgb[3][2]=28;
        ph_rgb[4][0]=181;
        ph_rgb[4][1]=211;
        ph_rgb[4][2]=51;
        ph_rgb[5][0]=132;
        ph_rgb[5][1]=195;
        ph_rgb[5][2]=65;
        ph_rgb[6][0]=77;
        ph_rgb[6][1]=183;
        ph_rgb[6][2]=73;
        ph_rgb[7][0]=51;
        ph_rgb[7][1]=169;
        ph_rgb[7][2]=75;
        ph_rgb[8][0]=34;
        ph_rgb[8][1]=180;
        ph_rgb[8][2]=107;
        ph_rgb[9][0]=10;
        ph_rgb[9][1]=184;
        ph_rgb[9][2]=182;
        ph_rgb[10][0]=70;
        ph_rgb[10][1]=144;
        ph_rgb[10][2]=205;
        ph_rgb[11][0]=56;
        ph_rgb[11][1]=83;
        ph_rgb[11][2]=164;
        ph_rgb[12][0]=90;
        ph_rgb[12][1]=81;
        ph_rgb[12][2]=162;
        ph_rgb[13][0]=99;
        ph_rgb[13][1]=69;
        ph_rgb[13][2]=157;
        ph_rgb[14][0]=70;
        ph_rgb[14][1]=44;
        ph_rgb[14][2]=131;


        double ph_0[]=new double[3];
        double ph_1[]=new double[3];
        double ph_2[]=new double[3];
        double ph_3[]=new double[3];
        double ph_4[]=new double[3];
        double ph_5[]=new double[3];
        double ph_6[]=new double[3];
        double ph_7[]=new double[3];
        double ph_8[]=new double[3];
        double ph_9[]=new double[3];
        double ph_10[]=new double[3];
        double ph_11[]=new double[3];
        double ph_12[]=new double[3];
        double ph_13[]=new double[3];
        double ph_14[]=new double[3];

        double ph_test[]=new double[3];

        RGBToLAB(ph_rgb[0][0],ph_rgb[0][1],ph_rgb[0][2],ph_0);
        RGBToLAB(ph_rgb[1][0],ph_rgb[1][1],ph_rgb[1][2],ph_1);
        RGBToLAB(ph_rgb[2][0],ph_rgb[2][1],ph_rgb[2][2],ph_2);
        RGBToLAB(ph_rgb[3][0],ph_rgb[3][1],ph_rgb[3][2],ph_3);
        RGBToLAB(ph_rgb[4][0],ph_rgb[4][1],ph_rgb[4][2],ph_4);
        RGBToLAB(ph_rgb[5][0],ph_rgb[5][1],ph_rgb[5][2],ph_5);
        RGBToLAB(ph_rgb[6][0],ph_rgb[6][1],ph_rgb[6][2],ph_6);
        RGBToLAB(ph_rgb[7][0],ph_rgb[7][1],ph_rgb[7][2],ph_7);
        RGBToLAB(ph_rgb[8][0],ph_rgb[8][1],ph_rgb[8][2],ph_8);
        RGBToLAB(ph_rgb[9][0],ph_rgb[9][1],ph_rgb[9][2],ph_9);
        RGBToLAB(ph_rgb[10][0],ph_rgb[10][1],ph_rgb[10][2],ph_10);
        RGBToLAB(ph_rgb[11][0],ph_rgb[11][1],ph_rgb[11][2],ph_11);
        RGBToLAB(ph_rgb[12][0],ph_rgb[12][1],ph_rgb[12][2],ph_12);
        RGBToLAB(ph_rgb[13][0],ph_rgb[13][1],ph_rgb[13][2],ph_13);
        RGBToLAB(ph_rgb[14][0],ph_rgb[14][1],ph_rgb[14][2],ph_14);

        RGBToLAB(r,g,b,ph_test);

        double dist[]=new double[15];

        dist[0]=Math.sqrt(Math.pow((ph_0[0]-ph_test[0]),2)+Math.pow((ph_0[1]-ph_test[1]),2)+Math.pow((ph_0[2]-ph_test[2]),2));
        dist[1]=Math.sqrt(Math.pow((ph_1[0]-ph_test[0]),2)+Math.pow((ph_1[1]-ph_test[1]),2)+Math.pow((ph_1[2]-ph_test[2]),2));
        dist[2]=Math.sqrt(Math.pow((ph_2[0]-ph_test[0]),2)+Math.pow((ph_2[1]-ph_test[1]),2)+Math.pow((ph_2[2]-ph_test[2]),2));
        dist[3]=Math.sqrt(Math.pow((ph_3[0]-ph_test[0]),2)+Math.pow((ph_3[1]-ph_test[1]),2)+Math.pow((ph_3[2]-ph_test[2]),2));
        dist[4]=Math.sqrt(Math.pow((ph_4[0]-ph_test[0]),2)+Math.pow((ph_4[1]-ph_test[1]),2)+Math.pow((ph_4[2]-ph_test[2]),2));
        dist[5]=Math.sqrt(Math.pow((ph_5[0]-ph_test[0]),2)+Math.pow((ph_5[1]-ph_test[1]),2)+Math.pow((ph_5[2]-ph_test[2]),2));
        dist[6]=Math.sqrt(Math.pow((ph_6[0]-ph_test[0]),2)+Math.pow((ph_6[1]-ph_test[1]),2)+Math.pow((ph_6[2]-ph_test[2]),2));
        dist[7]=Math.sqrt(Math.pow((ph_7[0]-ph_test[0]),2)+Math.pow((ph_7[1]-ph_test[1]),2)+Math.pow((ph_7[2]-ph_test[2]),2));
        dist[8]=Math.sqrt(Math.pow((ph_8[0]-ph_test[0]),2)+Math.pow((ph_8[1]-ph_test[1]),2)+Math.pow((ph_8[2]-ph_test[2]),2));
        dist[9]=Math.sqrt(Math.pow((ph_9[0]-ph_test[0]),2)+Math.pow((ph_9[1]-ph_test[1]),2)+Math.pow((ph_9[2]-ph_test[2]),2));
        dist[10]=Math.sqrt(Math.pow((ph_10[0]-ph_test[0]),2)+Math.pow((ph_10[1]-ph_test[1]),2)+Math.pow((ph_10[2]-ph_test[2]),2));
        dist[11]=Math.sqrt(Math.pow((ph_11[0]-ph_test[0]),2)+Math.pow((ph_11[1]-ph_test[1]),2)+Math.pow((ph_11[2]-ph_test[2]),2));
        dist[12]=Math.sqrt(Math.pow((ph_12[0]-ph_test[0]),2)+Math.pow((ph_12[1]-ph_test[1]),2)+Math.pow((ph_12[2]-ph_test[2]),2));
        dist[13]=Math.sqrt(Math.pow((ph_13[0]-ph_test[0]),2)+Math.pow((ph_13[1]-ph_test[1]),2)+Math.pow((ph_13[2]-ph_test[2]),2));
        dist[14]=Math.sqrt(Math.pow((ph_14[0]-ph_test[0]),2)+Math.pow((ph_14[1]-ph_test[1]),2)+Math.pow((ph_14[2]-ph_test[2]),2));






        twoMin(dist);

        for(int i = 0; i < 15; i++)
        {
//            Log.e("Distance from " + i + " = ", "" + dist[i]);
        }

        double ph_near, ph_far;
        ph_near = 0.0;
        ph_far = 0.0;
        double total=first+second;
        for(int x = 0; x < 15; x++)
        {
            if(dist[x] == first)
                ph_near = x;
            else if(dist[x] == second)
                ph_far = x;
        }
//        Log.e("Indexed: ", "" + ph_near + " - " + ph_far);

        double ph_value=first/total;

        if(ph_near < ph_far)
            ph_value=ph_value+ph_near;
        else
            ph_value=ph_near-ph_value;

//        System.out.println("The PH value is: "+ph_value);



        return ph_value;

    }

    private double predictSaturation(int r, int g, int b)
    {
        float[] hsv = rgb2hsv(r, g, b);
        float saturation = hsv[1];
        return (double)saturation;
    }



    public void twoMin(double a[])
    {

        int arr_size = a.length;

        /* There should be atleast two elements */
        if (arr_size < 2)
        {
//            System.out.println(" Invalid Input ");
            return;
        }

        first = second = Integer.MAX_VALUE;
        for (int i = 0; i < arr_size ; i ++)
        {
            /* If current element is smaller than first
              then update both first and second */
            if (a[i] < first)
            {
                second = first;
                first = a[i];
            }

            /* If arr[i] is in between first and second
               then update second  */
            else if (a[i] < second && a[i] != first)
                second = a[i];
        }
        /*if (second == Integer.MAX_VALUE)
            System.out.println("There is no second" +
                    "smallest element");
        else
            System.out.println("The smallest element is " +
                    first + " and second Smallest" +
                    " element is " + second);*/
    }



    private int findMinIdx(double[] numbers)
    {
        if (numbers == null || numbers.length == 0) return -1; // Saves time for empty array
        // As pointed out by ZouZou, you can save an iteration by assuming the first index is the smallest
        double minVal = numbers[0]; // Keeps a running count of the smallest value so far
        int minIdx = 0; // Will store the index of minVal
        for(int idx=1; idx<numbers.length; idx++)
        {
            if(numbers[idx] < minVal)
            {
                minVal = numbers[idx];
                minIdx = idx;
            }
        }
        return minIdx;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        try
        {
            dos.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public static String rgbToHex(int r, int g, int b)
    {
        String hexString = "#";
        if(r < 16)
            hexString = hexString + "0";
        hexString = hexString + Integer.toHexString(r);
        if(g < 16)
            hexString = hexString + "0";
        hexString = hexString + Integer.toHexString(g);
        if(b < 16)
            hexString = hexString + "0";
        hexString = hexString + Integer.toHexString(b);
        return hexString;
    }

    public static float[] rgb2hsv(int r, int g, int b)
    {
        float[] hsv = new float[3];
        Color.RGBToHSV(r, g, b, hsv);
        return hsv;

    }
}
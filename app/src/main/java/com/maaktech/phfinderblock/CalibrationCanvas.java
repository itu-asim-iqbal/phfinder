package com.maaktech.phfinderblock;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.View;

/**
 * Created by murtaza on 10/9/17.
 */

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class CalibrationCanvas extends View
{
    private float width;
    private float height;
    public static float centimeter;
    private static final float halfGridWidth = MainActivity.halfGridWidth;
    public static final int refDistanceX = 20;//300;
    public CalibrationCanvas(Context context, int w, int h)
    {
        super(context);
        width = w;
        height = h;
        centimeter = width / 6;
    }

    public void onDraw(Canvas canvas)
    {

        Paint paint = new Paint();
        paint.setColor(Color.RED);
        canvas.drawLine(0f, 0f, (width / 2.0f) - halfGridWidth,(height / 2.0f) - halfGridWidth, paint); // upper left
        canvas.drawLine(width, 0f, (width / 2.0f) + halfGridWidth, (height / 2.0f) - halfGridWidth, paint); // upper right
        canvas.drawLine(0f, height, (width / 2.0f) - halfGridWidth,(height / 2.0f) + halfGridWidth, paint); // lower left
        canvas.drawLine(width, height, (width / 2.0f) + halfGridWidth, (height / 2.0f) + halfGridWidth, paint); // lower right

        drawHollowRect((width / 2f) - halfGridWidth, (height / 2f) - halfGridWidth, (width / 2f) + halfGridWidth, (height / 2f) + halfGridWidth, paint, canvas);

        drawHollowRect((width / 2f) - (centimeter / 2f), (height / 2f) - (3 * centimeter), (width / 2f) + (centimeter / 2f), (height / 2f) + (3 * centimeter), paint, canvas);

        paint.setColor(Color.parseColor(MainActivity.rgbToHex(MainActivity.originalRefColors[0][0], MainActivity.originalRefColors[0][1], MainActivity.originalRefColors[0][2])));
        drawHollowRect(0f, (height / 2f) - (3 * centimeter), (width / 2f) - (centimeter / 2f) - 1, (height / 2f) - (2 * centimeter), paint, canvas);
        paint.setColor(Color.parseColor(MainActivity.rgbToHex(MainActivity.originalRefColors[1][0], MainActivity.originalRefColors[1][1], MainActivity.originalRefColors[1][2])));
        drawHollowRect(0f, (height / 2f) - (2 * centimeter), (width / 2f) - (centimeter / 2f) - 1, (height / 2f) - (1 * centimeter), paint, canvas);
        paint.setColor(Color.parseColor(MainActivity.rgbToHex(MainActivity.originalRefColors[2][0], MainActivity.originalRefColors[2][1], MainActivity.originalRefColors[2][2])));
        drawHollowRect(0f, (height / 2f) - (1 * centimeter), (width / 2f) - (centimeter / 2f) - 1, (height / 2f), paint, canvas);
        paint.setColor(Color.parseColor(MainActivity.rgbToHex(MainActivity.originalRefColors[3][0], MainActivity.originalRefColors[3][1], MainActivity.originalRefColors[3][2])));
        drawHollowRect(0f, (height / 2f), (width / 2f) - (centimeter / 2f) - 1, (height / 2f) + (1 * centimeter), paint, canvas);
        paint.setColor(Color.parseColor(MainActivity.rgbToHex(MainActivity.originalRefColors[4][0], MainActivity.originalRefColors[4][1], MainActivity.originalRefColors[4][2])));
        drawHollowRect(0f, (height / 2f) + (1 * centimeter), (width / 2f) - (centimeter / 2f) - 1, (height / 2f) + (2 * centimeter), paint, canvas);
        paint.setColor(Color.parseColor(MainActivity.rgbToHex(MainActivity.originalRefColors[5][0], MainActivity.originalRefColors[5][1], MainActivity.originalRefColors[5][2])));//#F6EB16
        drawHollowRect(0f, (height / 2f) + (2 * centimeter), (width / 2f) - (centimeter / 2f) - 1, (height / 2f) + (3 * centimeter), paint, canvas);

        paint.setColor(Color.parseColor("#FFFFFF"));

        canvas.drawCircle(refDistanceX, (height / 2f) - (2.5f * centimeter), 10f, paint);
        canvas.drawCircle(refDistanceX, (height / 2f) - (1.5f * centimeter), 10f, paint);
        canvas.drawCircle(refDistanceX, (height / 2f) - (0.5f * centimeter), 10f, paint);
        canvas.drawCircle(refDistanceX, (height / 2f) + (0.5f * centimeter), 10f, paint);
        paint.setColor(Color.parseColor("#000000"));
        canvas.drawCircle(refDistanceX, (height / 2f) + (1.5f * centimeter), 10f, paint);
        paint.setColor(Color.parseColor("#FFFFFF"));
        canvas.drawCircle(refDistanceX, (height / 2f) + (2.5f * centimeter), 10f, paint);

    }

    private void drawHollowRect(float startX, float startY, float endX, float endY, Paint paint, Canvas canvas)
    {
        canvas.drawLine(startX, startY, startX, endY, paint);
        canvas.drawLine(endX, startY, endX, endY, paint);
        canvas.drawLine(startX, startY, endX, startY, paint);
        canvas.drawLine(startX, endY, endX, endY, paint);
    }
}
